# API Agent

## Installation

```shell
npm i -g fastify-cli

git clone https://gitlab.com/devmate-training/ssj-nonthaburi/queue-api-agent.git
cd queue-api-agent
npm i
```

## Running

```shell
npm run dev
```

## Building

```shell
npm run build
```

## Runnig with `pm2`

official document website: https://pm2.keymetrics.io/docs/usage/quick-start/

### Install `pm2`

```shell
npm i -g pm2
```

running

```shell
pm2 start dist/server.js --name api-agent
```

`pm2` usage

```shell
pm2 list # list all process
pm2 stop <id> # stop process by <id>
pm2 restart <id> # restart process
pm2 logs <id> # view logs
pm2 delete <id> # remove process
pm2 save # save process states
pm2 startup # auto start when os started
```

## Build docker image

```shell
docker build -t xxxx/ssjnon-api-agent .
```

`xxxx` is docker hub id

## Docker deploy

in `deploy` folder:

- `docker-compose.yml` change port (exp: `3333`)
- `config/.env` change database connection environment

how to run in development mode

```shell
cd deploy
```

```shell
docker compose up --force-recreate
```

run in production mode

```shell
docker compose up -d --force-recreate
```
