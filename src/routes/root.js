'use strict';

module.exports = async function (fastify, opts) {
	fastify.get('/', async function (request, reply) {
		return { root: true };
	});
	// Start job
	fastify.get(
		'/start',
		{
			onRequest: fastify.basicAuth,
		},
		async function (request, reply) {
			const visitJob = fastify.cron.getJobByName('getVisit');
			visitJob.start();
			const queueJob = fastify.cron.getJobByName('getQueue');
			queueJob.start();
			return { start: true };
		}
	);
	// Stop job
	fastify.get(
		'/stop',
		{
			onRequest: fastify.basicAuth,
		},
		async function (request, reply) {
			fastify.cron.stopAllJobs();
			return { stop: true };
		}
	);
};
