'use strict';

const fp = require('fastify-plugin');

module.exports = fp(async function (fastify, opts) {
	fastify.register(require('@fastify/basic-auth'), {
		validate: function (username, password, req, reply, done) {
			if (username === 'admin' && password === 'admin') {
				done();
			} else {
				done(new Error('Invalid credentials'));
			}
		},
		authenticate: { realm: 'Queue Management' },
	});
});
