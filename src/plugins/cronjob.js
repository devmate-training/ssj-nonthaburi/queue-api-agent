'use strict';

const fp = require('fastify-plugin');

module.exports = fp(async function (fastify, opts) {
	fastify.register(require('fastify-cron'), {
		jobs: [
			{
				name: 'getVisit',
				cronTime: '* * * * *', // Every minute
				onTick: async (server) => {
					const db = fastify.db;
					try {
						const results = await db('patient_visit');
						if (!results) {
							return;
						}
						// fetch
						// Ingress API
						const url = `${process.env.API_URL}/queues/visit`;
						const response = await fetch(url, {
							method: 'POST',
							body: JSON.stringify(results),
							headers: {
								'Content-Type': 'application/json',
								Authorization: 'Basic ' + process.env.API_KEY,
							},
						});

						if (response.ok) {
							//
						} else {
							console.error(response.status);
						}
					} catch (error) {
						console.error(error);
					}
				},
				startWhenReady: true,
			},
			{
				name: 'getQueue',
				cronTime: '*/5 * * * * *', // Every 5 seconds
				onTick: async (server) => {
					const db = fastify.db;
					try {
						const results = await db('queue_caller');

						if (!results) {
							return;
						}
						// fetch
						// Ingress API
						const url = `${process.env.API_URL}/queues`;
						const response = await fetch(url, {
							method: 'POST',
							body: JSON.stringify(results),
							headers: {
								'Content-Type': 'application/json',
								Authorization: 'Basic ' + process.env.API_KEY,
							},
						});

						if (response.ok) {
							//
						} else {
							console.error(response.status);
						}
					} catch (error) {
						console.error(error);
					}
				},
				startWhenReady: true,
			},
		],
	});
});
