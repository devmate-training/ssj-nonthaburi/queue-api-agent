FROM node:20-alpine AS builder
LABEL maintainer="Satit Rianpit <rianpit@gmail.com>"
WORKDIR /app
COPY . .
RUN npm i
RUN npm run build

FROM node:20-alpine
WORKDIR /app
COPY --from=builder /app/dist .
COPY --from=builder /app/package*.json .
COPY --from=builder /app/process.json .
RUN npm i --only=production
RUN npm install pm2 -g
EXPOSE 3000
CMD ["pm2-runtime", "process.json"]